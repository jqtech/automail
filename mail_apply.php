<?php
	
	$mailer_data_firstlogin = 'admin'; //username that used if $mailer_file_datasignin not exists
	$mailer_data_firstpassw = '12345'; //password that used if $mailer_file_datasignin not exists
	
	$mailer_file_format = '_config_%%filename%%.noext.php'; //don't change %%filename%% text!
	
	$mailer_file_datasignin = 'signin'; //for storing ui-page data authenticate
	$mailer_file_dataserver = 'server'; //for storing mail server configuration
	$mailer_file_datanaming = 'naming'; //for storing attribute variable naming
	$mailer_file_datarespon = 'respon'; //for storing mail auto respond message
	
	$mailer_file_encryptkey = 'jasminne@freelancer.com'; //for string used by encription data in file
	
	//don't change this bellow!
	$mailer_json_defaultattr = '{"$$mail_addr$$":"$$mail_addr$$","$$fron_name$$":"$$fron_name$$","$$last_name$$":"$$last_name$$","$$full_name$$":"$$full_name$$","$$mail_subj$$":"$$mail_subj$$","$$mail_quot$$":"$$mail_quot$$","$$mail_date$$":"$$mail_date$$","$$sent_date$$":"$$sent_date$$"}';
	$mailer_json_defaultauth = '{"incoming_serv":"","incoming_port":"","outgoing_serv":"","outgoing_port":"","incoming_user":"","incoming_pass":"","outgoing_user":"","outgoing_pass":"","outgoing_type":"","outgoing_test":"mailer-daemon@googlemail.com","reply_to_mail":"","reply_to_name":""}';
	function mcrypt_encode($value,$forfile=true) {
		global $mailer_file_encryptkey;
		$key = $mailer_file_encryptkey;
		$return = $forfile? '<?php exit; ?>' : '';
		$return.= base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), trim($value), MCRYPT_MODE_CBC, md5(md5($key))));
		return $return;
	}
	function mcrypt_decode($value,$forfile=true) {
		global $mailer_file_encryptkey;
		$key = $mailer_file_encryptkey;
		$return = $forfile? str_replace('<?php exit; ?>','',$value) : $value;
		$return = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($return), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
		return urldecode(trim($return));
	}
	function file_naming($value) {
		global $mailer_file_format;
		return str_ireplace('%%filename%%',urldecode($value),$mailer_file_format);
	}
	function calc_time($time) {
		if ($time==false) return 'Never';
		$time = time() - ((int)$time);
		$tokens = array (
			31536000 => 'Year',
			2592000 => 'Month',
			604800 => 'Week',
			86400 => 'Day',
			3600 => 'Hour',
			60 => 'Minute',
			1 => 'Second'
		);
		foreach ($tokens as $unit => $text) {
			if ($time < $unit) continue;
			$numberOfUnits = floor($time / $unit);
			return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'').' Ago';
		}
	}
	
	if (isset($_REQUEST['act'])) {
	
		$data = isset($_REQUEST['data'])? base64_decode($_REQUEST['data']) : '';
		$json = json_decode(urldecode(trim($data)), true);
		//var_dump(nl2br(base64_decode($_REQUEST['data'])));
		if ($json !== null) 
		{
			//var_dump($json);
			$fpointer = fopen(file_naming($_REQUEST['act']), "w");
			$echo = fwrite($fpointer,mcrypt_encode($data));
			fclose($fpointer);
			
			echo $echo;
		}
	
	}

	
?>