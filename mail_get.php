<?php

	error_reporting(E_ALL^E_WARNING^E_NOTICE);
	include "mail_response.php";
	include "mail_apply.php";
	
	$serv_naming = file_naming($mailer_file_dataserver);
	if (file_exists($serv_naming)) {
		$fpointer = fopen($serv_naming, "r");
		$jsondata = fread($fpointer, filesize($serv_naming));
		$jsondata = json_decode(mcrypt_decode($jsondata), true);
		fclose($fpointer);
		if ($jsondata===null) unset($jsondata);
	}
	
	$test_conn = false;
	if (isset($_REQUEST['test'])) {
		$test_conn = true;
		$jsondata = isset($_REQUEST['test'])? base64_decode($_REQUEST['test']) : '';
		$jsondata = json_decode(trim($jsondata), true);
		if ($jsondata === null) die("failed parsing data, configuration value may have unsafe character");
	}
	$jsondata = isset($jsondata)? $jsondata : json_decode($mailer_json_defaultauth,true);
	
	/* auth setting */
	$reply_to_email		= $jsondata['reply_to_mail'];//'mailer@edosk.com';
	$reply_to_name		= $jsondata['reply_to_name'];//'edosk mailer;
	$incoming_server	= $jsondata['incoming_serv'];//'mail.edosk.com';
	$incoming_port		= $jsondata['incoming_port'];//'143'; # default pop:110 imap:143
	$incoming_user		= $jsondata['incoming_user'];//'mailer@edosk.com';
	$incoming_pass		= $jsondata['incoming_pass'];//'Sn"IaaCi';
	$outgoing_server	= $jsondata['outgoing_serv'];//'mail.edosk.com';
	$outgoing_port		= $jsondata['outgoing_port'];//'25'; # default 25
	$outgoing_user		= $jsondata['outgoing_user'];//'mailer@edosk.com';
	$outgoing_pass		= $jsondata['outgoing_pass'];//'Sn"IaaCi';
	$outgoing_type		= $jsondata['outgoing_type'];//'default';
	$outgoing_test		= $jsondata['outgoing_test'];//'mailer-daemon@googlemail.com';
	$connection_type	= 'imap';//'novalidate-cert';	# pop/imap

	/* create a object of the class */
	$response = new autoresponse($incoming_user,$incoming_pass,$reply_to_email,$reply_to_name,$incoming_server,$connection_type,$incoming_port,$outgoing_user,$outgoing_pass,$outgoing_server,$outgoing_port,$outgoing_type);

	if (isset($cronmailer)) {
		$cronmailer = $response;
		unset($response);
	} else {
		/* test outgoing */
		if ($test_conn && strtolower($_REQUEST['task'])=='outgoing') {
			ob_start();
			$test_send = $response->mail($outgoing_test,"outgoing test","test successed...",false,false,true);
			$outlog = ob_get_contents();
			ob_end_clean();
			if ($test_send) $outlog = "connection success";
			echo($outlog);
			exit;
		}
	
		/* connect to the server */
		$folder = isset($_REQUEST['dir'])&&$_REQUEST['dir']!='INBOX'? 'INBOX.'.trim($_REQUEST['dir']) : 'INBOX';
		$connect = $response->connect($folder);
		if ($connect===false) exit;
		
		/* test incoming */
		if ($test_conn) {
			echo("connection success");
			$response->close_mailbox();
			exit;
		}
		
		$filter = strtoupper($folder)!='INBOX'? '':'Autoreply:';

		$find = isset($_REQUEST['find'])? trim($_REQUEST['find']) : 'ALL';
		//$list = $response->search($find,$filter,!empty($_POST['delete']));
		$list = $response->paging($find,$filter,!empty($_POST['delete']),isset($_REQUEST['iDisplayStart'])?$_REQUEST['iDisplayStart']:null,isset($_REQUEST['iDisplayLength'])?$_REQUEST['iDisplayLength']:null);
		
		if ($list!==false) {			
			//Output
			$total = $list['chck']->Nmsgs;
			unset($list['chck']);
			$output = array(
				"sEcho" => isset($_REQUEST['sEcho'])?intval($_REQUEST['sEcho']):null,
				"iTotalRecords" => $total,
				"iTotalDisplayRecords" => $total,
				"mail" => array()
			);
			
			//var_dump($list); exit;
			//$numb = count($list);
			//$output = array(
			//	"mail" => array()	
			//);
			//foreach (array_reverse($list) as $rec) {
			foreach ($list as $rec) {
				$innerData = array();
				
				$numb = $rec['head']->msgno;
				$innerData['sender_numb']	= $numb;
				$innerData['sender_date']	= trim( date("d/M/Y h:i A",$rec['head']->udate) );
				$innerData['sender_name']	= $rec['head']->from;//isset($rec['head']->from[0]->personal)? trim( $rec['head']->from[0]->personal ) : trim( $rec['head']->from[0]->mailbox );
				$dt_mail = $rec['head']->from;//trim( $rec['head']->from[0]->mailbox.'@'.$rec['head']->from[0]->host );
				$dt_text = trim( nl2br($rec['body']) );
				$hidden_ = '<button class="btn pull-right" data-email="'.str_replace('"','&quot;',$dt_mail).'" data-value="'.base64_encode($dt_text).'" onClick="subrowshow(this,\''.str_replace('inbox.','',strtolower($folder)).'\',\''.$numb.'\')">Read &raquo;</button>';
				$innerData['sender_subj']	= trim( str_ireplace('Autoreply:','',str_ireplace('Re:','',$rec['head']->subject)) ) . $hidden_;
				
				$output['mail'][] = $innerData;
				//$numb--;
			}
			
			header('Content-type: application/json');
			echo json_encode( $output );
		}

		/* close the mailbox */
		$response->close_mailbox();
	}

?>