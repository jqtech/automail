<?php

	error_reporting(E_ALL);
	# in linux this script needs +x (exec permission)

	$cronmailer = true;
	include 'mail_get.php';
	
	//connect to the server
	$connect = $cronmailer->connect();
	if (isset($croncalls) && $connect===false) exit;
	
	//read respond template
	$responds_naming = file_naming($mailer_file_datarespon);
	if (file_exists($responds_naming)) {	
		$fpointer = fopen($responds_naming, "r");
		$jsontemplates = fread($fpointer, filesize($responds_naming));
		$jsontemplates = json_decode(mcrypt_decode($jsontemplates), true);
		fclose($fpointer);
		if ($jsontemplates===null) unset($jsontemplates);
	}
	$jsontemplates = isset($jsontemplates)? $jsontemplates : array();
	
	//read attribute naming
	$attr_naming = file_naming($mailer_file_datanaming);
	if (!file_exists($attr_naming)) {
		$fpointer = fopen($attr_naming, "w");
		fwrite($fpointer, mcrypt_encode($mailer_json_defaultattr));
		fclose($fpointer);
		$jsonattribute = json_decode($mailer_json_defaultattr, true);
	} else {
		$fpointer = fopen($attr_naming, "r");
		$jsonattribute = fread($fpointer, filesize($attr_naming));
		$jsonattribute = json_decode(mcrypt_decode($jsonattribute), true);
		fclose($fpointer);
		if ($jsonattribute===null) unset($jsonattribute);
	}
	$jsonattribute = isset($jsonattribute)? $jsonattribute : json_decode($mailer_json_defaultattr,true);
	
	if (!isset($croncalls)) {
		//send the response mails
		if (empty($_REQUEST['debug'])) $cronmailer->send($jsontemplates,$jsonattribute);
		
		if (isset($_REQUEST['debug'])) $cronmailer->debugging();

		//close the mailbox
		$cronmailer->close_mailbox();
	}
	

?>