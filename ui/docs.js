jQuery(document).ready(function($) {
	//function for adjust left menu position
	$('.bs-docs-sidenav').affix({
		offset: {
			top: function() {
			  return $(window).width() <= 767 ? 290 : 0
			},
			bottom: 50
		}
	});
	//function for initialize time picker
	$('.timepicker').live('mouseover', function(){
		$(this).timepicker({
			minuteStep: 1,
			secondStep: 1,
			showInputs: true,
			showSeconds: true,
			showMeridian: false,
			disableFocus: true,
			defaultTime: 'value',
			template: 'dropdown',
			modalBackdrop: false
		});
	});
	//function for attachment upload button
	new AjaxUpload('#uploadattachment', {
		action: 'index.php?upload',
		onSubmit : function(){
			$('#uploadattachment').text('Uploading...');
		},
		onComplete : function(file){
			alert('File uploaded...');
			document.location.href = document.location.href;
		}		
	});
	$('.signout').bind('mouseover', function(){
		$(this).addClass('active');
	});
	$('.signout').bind('mouseleave', function(){
		$(this).removeClass('active');
	});
	
	last_exec();
	setInterval(function(){last_exec()}, 50000);
});
//read last execution script
var last_exec = function(){
	$.get('index.php?logtime', function(status){
		if ($.trim(status)=='') return;
		$('#lastexec').text(status);
	})
}

//exec script manually
var manual_exec = function(obj){
	//$(obj).attr('onClick','');
	$('#lastexec').text('Executing');
	$.get(mailer_file_datarunner, function(status){
		//document.location.reload();
		last_exec();
	})
}

//function for refreshing spy-scroll
var menupos = function(){
	$('[data-spy="scroll"]').each(function () {
		var $spy = $(this).scrollspy('refresh')
	});
}

//--------------------------------------------------------------- INCOMING/OUTGOING RESPONDER > DATATABLES

var emptytable = function(folder) {
	var sect = '';
	if (folder=='inbox') sect = 'Inbox';
	if (folder=='autoignore') sect = 'Ignored';
	if (folder=='autoreply') sect = 'Incoming';
	if (folder=='autosent') sect = 'Outgoing';
	if (folder=='autosenfail') sect = 'Failed';
	if (!confirm('Are you sure to delete all '+sect+' Emails ?')) return;
	inittable(folder,true);
}

//function for initialize datatables
var inittable = function(folder,isDelete) {
	var obj, grp = $('.btn-'+folder);
	$('#'+folder).dataTable({
		'bDestroy': true,
		'bProcessing': true,
		'bServerSide': true,
		'bSort': false,
		'bFilter': false,
		'sAjaxSource': mailer_file_datagetter+'?dir='+folder.toUpperCase(),
		'sAjaxDataProp': 'mail',
		'aaSorting': [],
		'aoColumns': [
			{ 'mDataProp': 'sender_numb' },
			{ 'mDataProp': 'sender_name' },
			{ 'mDataProp': 'sender_date' },
			{ 'mDataProp': 'sender_subj' }
		],
		'fnServerData': function(sSource,aoData,fnCallback) {
			$('#'+folder).unbind('draw');
			$('#'+folder).bind('draw', function(){ menupos() });
			$('#'+folder+'_processing').css({ height:'40px'});
			$('#'+folder+'_processing').html('');
			$(grp).children().attr('disabled',true);
			$(grp).appendTo('#'+folder+'_processing');
			if (isDelete==true) {
				aoData.push({name:'delete',value:true})
				var obj = $('.btn-'+folder).children('.tbl-del');
				$(obj).text('Deleting...');
			} else {
				obj = $('.btn-'+folder).children('.tbl-fet');
				$(obj).text('Fetching...');
			}
			$.ajax({
				'dataType': 'json',
				'type': isDelete==true?'POST':'GET',
				'url': sSource,
				'data': aoData,
				'success': function(data) {
					$('#'+folder+'_chart').hide();
					initchart(folder,data);
					$('<br/>').appendTo($('#'+folder+'_info').parent());
					$(grp).appendTo($('#'+folder+'_info').parent());
					$(grp).children().attr('disabled',false);
					if (isDelete==true) {
						$(obj).text('Delete Mail');
					} else {
						$(obj).text('Fetch Mail');
					}
					fnCallback(data);
				},
				'timeout': 0,
				'error': function(xhr,textStatus,error) {
					errorStatus = xhr.responseText? xhr.responseText.replace('<br','<input style="display:none"') : '';
					$(grp).children().attr('disabled',false);
					$(grp).popover({
						'selector': true,
						'animation': true,
						'html': true,
						'placement': 'top',
						'trigger': 'manual',
						'title': textStatus,
						'content': '<div style="width:100%;overflow:hidden;word-wrap:break-word">'+errorStatus+'</div>'
					});
					if (isDelete==true) {
						$(obj).text('Delete Mail');
					} else {
						$(obj).text('Fetch Mail');
					}
					$(grp).popover('show');
				}
			});
		}
	});
}
//function for showing sub-row on datatables
var subrowshow = function(obj,folder,unix){
	var nTr = $(obj).parents('tr')[0];
	var oTable = $('#'+folder).dataTable();
	if (  oTable.fnIsOpen(nTr) ) {
		$(obj).html('Read &raquo;');
		oTable.fnClose( nTr );
	} else {
		$(obj).html('&laquo; Close');
		oTable.fnOpen( nTr, function(){ return '' +
				'<table style="width:100%; border: silver solid 1px" class="subshow_' + folder + '_' + unix + ' dataTable"><tr><td align="center">' + 
					'<h5 style="border-bottom: silver solid 1px">' +
						'Email: <a href="mailto:' + $(obj).attr('data-email') + '">' + $(obj).attr('data-email') + '</a>' +
					'</h5>' +
					atob($(obj).attr('data-value')) + 
				'</td></tr></table>';
		}, folder+ '_details_' + unix );
	}
	menupos();
}
//function for chart rendering
var initchart = function(folder,data) {
	var chart = {};
	var array = [];
	$.each(data.mail, function(i, val) {
		key = val.sender_date.indexOf(' ');
		if (key >= 0) {
			key = val.sender_date.substring(0,key);
			if (chart[key]==undefined) chart[key] = 1;
			else chart[key] = chart[key] + 1;
		}
	});
	$.each(chart, function(i, val) { array.push([i,val]); });
	if (array.length>1) {
		$('#'+folder+'_chart').show();
		$('#'+folder+'_chart').empty().jqplot([array], {
			animate: true,
			animateReplot: true,
			cursor: {
				show: true,
				zoom: true,
				looseZoom: true,
				showTooltip: false
			},
			axes:{
				xaxis:{
					renderer:$.jqplot.DateAxisRenderer
					, min:array[array.length-1][0]
					, max:array[0][0]
				}
			},
			series:[{lineWidth:4, showMarker:false, markerOptions:{style:'square'}, fill: false}]
		});
	}
}

//--------------------------------------------------------------- SETTING RESPONDER > SECTION

//function for showing saving status
var saveresultstatus = function(classname,type,message) {
	var alert = '' + 
		'<div class="alert-of-'+classname+' row-fluid">' +
		'	<div class="alert ' + type + ' fade in">' +
		'		<button type="button" class="close" data-dismiss="alert"><i class="icon-remove-sign" style="margin-top:6px"></i></button>' +
		'		'+ message +
		'	</div>' +
		'</div>';
	$('.alert-of-'+classname).remove();
	$('.'+classname).before($(alert).fadeIn());
}

//--------------------------------------------------------------- ATTRIBUTE VARIABLE > SECTION

//function for saving attribute setting
var saveattributelist = function() {
	var messg = {};
	var title = "<strong>Saving Attribute Variable</strong>";
	$('.attribute-data').find('input').each(function(){
		var obj = $(this);
		var key = $(obj).attr('placeholder');
		messg[key] = $.trim($(obj).val())===''? key : $(obj).val();
	});
	messg = JSON.stringify(messg);
	var button = $('.attribute-data').find('.btn-success')[0];
	$(button).attr('disabled',true);
	$(button).text('Saving...');
	$.post(mailer_file_datasaving+'?act='+mailer_file_datanaming, {'data':btoa(messg)}, function(status){
		if (parseInt(status)>0) {
			saveresultstatus('attribute-act','alert-success',title+' successfully...');
		} else {
			var logfail = '<p style="padding-left:20px">'+status+'</p></div>';
			saveresultstatus('attribute-act','alert-danger',title+' failed, please try again...'+logfail);
		}
		$(button).attr('disabled',false);
		$(button).text('Save Change');
	}).error(function(msg){
		saveresultstatus('attribute-act','alert-danger',title+' error: '+msg.statusText+' ('+msg.status+')');
		$(button).attr('disabled',false);
		$(button).text('Save Change');
	});
}

//--------------------------------------------------------------- MAIL CONFIGURATION > SECTION

//function for saving server configuration
var savemailserver = function(_this) {
	var messg = {};
	var title = "<strong>Saving Server Configuration</strong>";
	$('.mailserver-data').find('input,select').each(function(){
		var obj = $(this);
		var key = $(obj).attr('servkeys');
		messg[key] = $.trim($(obj).val());
	});
	messg = JSON.stringify(messg);
	var button = _this;//$('.mailserver-data').find('.btn-success')[0];
	$(button).attr('disabled',true);
	$(button).text('Saving...');
	$.post(mailer_file_datasaving+'?act='+mailer_file_dataserver, {'data':btoa(messg)}, function(status){
		if (parseInt(status)>0) {
			saveresultstatus('mailserver-act','alert-success',title+' successfully...');
		} else {
			var logfail = '<p style="padding-left:20px">'+status+'</p></div>';
			saveresultstatus('mailserver-act','alert-danger',title+' failed, please try again...'+logfail);
		}
		$(button).attr('disabled',false);
		$(button).text('Save Change');
	}).error(function(msg){
		saveresultstatus('mailserver-act','alert-danger',title+' error: '+msg.statusText+' ('+msg.status+')');
		$(button).attr('disabled',false);
		$(button).text('Save Change');
	});
}

//function for test server connection
var testconserver = function(_this,task) {
	var messg = {};
	var title = "<strong>Testing Server Connection</strong>";
	$('.mailserver-data').find('input,select').each(function(){
		var obj = $(this);
		var key = $(obj).attr('servkeys');
		messg[key] = $.trim($(obj).val());
	});
	messg = JSON.stringify(messg);
	var button = _this;//$('.mailserver-data').find('.btn-success')[0];
	$(button).attr('disabled',true);
	$(button).text('Testing...');
	$.post(mailer_file_datagetter, {'test':btoa(messg), 'task':$(_this).attr('task')}, function(status){
		var logstat = '<p style="padding-left:20px">'+status+'</p></div>';
		saveresultstatus('mailserver-act','alert',title+logstat);
		$(button).attr('disabled',false);
		$(button).text('Test '+$(_this).attr('task'));
	}).error(function(msg){
		saveresultstatus('mailserver-act','alert-danger',title+' error: '+msg.statusText+' ('+msg.status+')');
		$(button).attr('disabled',false);
		$(button).text('Test '+$(_this).attr('task'));
	});
}

//--------------------------------------------------------------- LOGIN DETAILS > SECTION

var savesignindata = function() {
	var messg = {};
	var title = "<strong>Saving New Login Details</strong>";
	messg['username'] = $('#newusername').val();
	messg['password'] = $('#newpassword').val();
	messg = JSON.stringify(messg);
	var button = $('.signin-data').find('.btn-success')[0];
	$(button).attr('disabled',true);
	$(button).text('Saving...');
	//-- #testing old data --
	$.post('index.php', {'alter':$('#oldpassword').val()}, function(status){
		if (parseInt(status)==0) {
			//-- #saving new data --
			$.post(mailer_file_datasaving+'?act='+mailer_file_datasignin, {'data':btoa(messg)}, function(status){
				if (parseInt(status)>0) {
					saveresultstatus('signin-act','alert-success',title+' successfully...');
					//-- #bringing relogin --
					setInterval(function(){
						alert('Redirecting to login page');
						document.location.href = 'index.php?logout';
					},3000);
					//-- /bringing relogin --
				} else {
					var logfail = '<p style="padding-left:20px">'+status+'</p></div>';
					saveresultstatus('signin-act','alert-danger',title+' failed, please try again...'+logfail);
				}
				$(button).attr('disabled',false);
				$(button).text('Save Change');
			}).error(function(msg){
				saveresultstatus('signin-act','alert-danger',title+' error: '+msg.statusText+' ('+msg.status+')');
				$(button).attr('disabled',false);
				$(button).text('Save Change');
			});
			//-- /saving new data --
		} else {
			$('#oldpassword').popover('show');
			$(button).attr('disabled',false);
			$(button).text('Save Change');
		}
	}).error(function(msg){
		saveresultstatus('signin-act','alert-danger',title+' error: '+msg.statusText+' ('+msg.status+')');
		$(button).attr('disabled',false);
		$(button).text('Save Change');
	});
}

//--------------------------------------------------------------- RESPONSE MESSAGE > SECTION

//function for saving responder setting
var saverespondlist = function() {
	var count = -1;
	var messg = {};
	var title = "<strong>Saving Respond Message</strong>";
	$('.respond-data').each(function(){
		var obj = $(this);
		if (count >= 0) {
			messg[count] = {};
			messg[count]['no'] = $($(obj).find('.respond-order')[0]).val();
			messg[count]['t1'] = $($(obj).find('.timepicker')[0]).val();
			messg[count]['t2'] = $($(obj).find('.timepicker')[1]).val();
			messg[count]['tx'] = $($(obj).find('textarea')[0]).val();
		}
		count++;
	});
	messg['count'] = count;
	messg['black'] = $($('.respond-act').find('input.blacklist')[0]).val();
	messg['delay'] = $($('.respond-act').find('input.timepicker')[0]).val();
	messg['limit'] = $($('.respond-act').find('input.maxlimit')[0]).val();
	messg['using'] = $($('.respond-act').find('input[type=checkbox]')[0]).prop('checked');
	messg = JSON.stringify(messg);
	var button = $('.respond-act').find('.btn-success')[0];
	$(button).attr('disabled',true);
	$(button).text('Saving...');
	$.post(mailer_file_datasaving+'?act='+mailer_file_datarespon, {'data':btoa(encodeURIComponent(messg))}, function(status){
		if (parseInt(status)>0) {
			saveresultstatus('respond-act','alert-success',title+' successfully...');
		} else {
			var logfail = '<p style="padding-left:20px">'+status+'</p></div>';
			saveresultstatus('respond-act','alert-danger',title+' failed, please try again...'+logfail);
		}
		$(button).attr('disabled',false);
		$(button).text('Save Change');
	}).error(function(msg){
		saveresultstatus('respond-act','alert-danger',title+' error: '+msg.statusText+' ('+msg.status+')');
		$(button).attr('disabled',false);
		$(button).text('Save Change');
	});
}
//function for showing new respond form
var addrespondlist = function() {
	var tmp = $('.respond-data')[0];
	var tot = $(tmp).find('.respond-order option').length;
	$('.respond-order').append('<option>'+tot+'</option>');
	
	var add = $(tmp).clone().appendTo('.respond-list');
	$(add).show();
	menupos();
	
	var sel = $(add).find('.respond-order')[0];
	$(sel).val(tot);
}
//function for removing new respond form
var delrespondlist = function(obj) {
	if (confirm('Are you sure to delete this response ?')) {
		var par = $(obj).parent().parent().parent()[0];
		$(par).remove();
		menupos();
	}
}
//fucntion for deleting attachment
var removeattachment = function(obj,file) {
	if (confirm('Are you sure to delete this file ?')) {
		var button = $(obj);
		$(button).attr('disabled',true);
		$.post('index.php', {'remfile':file}, function(status){
			if (parseInt(status)>0) {
				alert('File removed...');
				document.location.href = document.location.href;
			}
		}).error(function(msg){
			alert('Failed, please try again...');
			$(button).attr('disabled',false);
		});
	}
}
