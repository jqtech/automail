<?php
	
	$runner_file = '../mail.php';
	$mailer_file = '../mail_get.php';
	$saving_file = '../mail_apply.php';
	
	session_start(); 
	require_once $saving_file;
	
	$saving_path = strrpos($saving_file,'/');
	$saving_path = substr($saving_file,0,$saving_path);
	$saving_path = trim($saving_path)!=''? $saving_path.'/' : '';
	
	if (isset($_REQUEST['logout'])) {
		unset($_SESSION['login']);
		Header('Location: index.php');
		exit;
	}
	if (isset($_REQUEST['logout'])) {
		unset($_SESSION['login']);
		Header('Location: index.php');
		exit;
	}
	if (isset($_FILES['userfile'])) {
		if (!is_writable($saving_path.'attachment')) mkdir($saving_path.'attachment');
		echo @copy($_FILES['userfile']['tmp_name'], $saving_path.'attachment/'.$_FILES['userfile']['name']);
		exit;
	}
	if (isset($_REQUEST['remfile'])) {
		if (file_exists($saving_path.'attachment/'.$_REQUEST['remfile'])) echo @unlink($saving_path.'attachment/'.$_REQUEST['remfile']);
		exit;
	}
	if (isset($_REQUEST['logtime'])) {
		echo calc_time(@file_get_contents($saving_path.'mail_exec.log'));
		exit;
	}
	//login authantication
	if (isset($_REQUEST['login']) || isset($_REQUEST['alter'])) {
		$signin_naming = $saving_path.file_naming($mailer_file_datasignin);
		if (!file_exists($signin_naming)) {
			$uipage_username = $mailer_data_firstlogin;
			$uipage_password = $mailer_data_firstpassw;
		} else {
			$fpointer = fopen($signin_naming, "r");
			$jsondata = fread($fpointer, filesize($signin_naming));
			$jsondata = json_decode(mcrypt_decode($jsondata), true);
			fclose($fpointer);
			if ($jsondata!==null) {
				$uipage_username = $jsondata["username"];
				$uipage_password = $jsondata["password"];
			} else {
				$uipage_username = $mailer_data_firstlogin;
				$uipage_password = $mailer_data_firstpassw;
			}
			unset($jsondata);
		}
		$inputed_username = isset($_REQUEST['username'])? $_REQUEST['username'] : '';
		$inputed_password = isset($_REQUEST['password'])? $_REQUEST['password'] : '';
		if (!isset($_REQUEST['login'])) {
			$inputed_username = $uipage_username;
			$inputed_password = $_REQUEST['alter'];
		}
		if ($uipage_username == $inputed_username && $uipage_password == $inputed_password) {
			if (!isset($_REQUEST['login'])) die("0");
			$_SESSION['login'] = $inputed_password;
			setcookie('ui-username', $inputed_username, (time()+3600*24*30*12)); //a year remmember
			if (isset($_REQUEST['remember'])) {
				setcookie('ui-password', mcrypt_encode($inputed_password,false), (time()+3600*24*7)); //a week remmember
			} else {
				setcookie('ui-password', '', time()-86400);
			}
			Header('Location: index.php');
		} else {
			if (!isset($_REQUEST['login'])) die("1");
			$wrongpwd = true;
		}
	}
	$login = isset($_SESSION['login'])? true : false;
		
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>MailBox Responder</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	
	<script src="bootstrap/js/jquery.min.js"></script>
	<script src="bootstrap/js/ajaxupload.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/bootstrap.timepicker.min.js"></script>
	<script src="bootstrap/js/dataTables.min.js"></script>
	<script src="bootstrap/js/dataTables.bootstrap.min.js"></script>
	<script src="bootstrap/js/jqplot.min.js"></script>
	<script src="bootstrap/js/jqplot.cursor.min.js"></script>
	<script src="bootstrap/js/jqplot.dateAxisRenderer.min.js"></script>
	<script src="docs.js"></script>
	

    <!-- styles -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
	<!--<link href="bootstrap/css/bootswatch.spacelab.css" rel="stylesheet">-->
	<!--<link href="bootstrap/css/bootswatch.cerulian.css" rel="stylesheet">-->
	<link href="bootstrap/css/bootswatch.united.css" rel="stylesheet">
	<style type="text/css">
		.navbar .nav>li>a {
			border: 0;
		}
		.navbar-inverse .nav .active>a, .navbar-inverse .nav .active>a:hover, .navbar-inverse .nav .active>a:focus {
			background-color: transparent;
		}
		.navbar-inverse .navbar-inner {
			background-color: #d44413;
			background-image: -moz-linear-gradient(top, #ce4213, #dd4814);
			background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ce4213), to(#dd4814));
			background-image: -webkit-linear-gradient(top, #ce4213, #dd4814);
			background-image: -o-linear-gradient(top, #ce4213, #dd4814);
			background-image: linear-gradient(to bottom, #ce4213, #dd4814);
			background-repeat: repeat-x;
			filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffce4213', endColorstr='#ffdd4814', GradientType=0);
			border: 1px solid #c64012;
		}
	</style>
	<!--<link href="bootstrap/css/bootswatch.simplex.css" rel="stylesheet">-->
	<!--<link href="bootstrap/css/bootswatch.readable.css" rel="stylesheet">-->
	<!--<link href="bootstrap/css/bootswatch.amelia.css" rel="stylesheet">-->
	<!--<link href="bootstrap/css/bootswatch.cyborg.css" rel="stylesheet">-->
	<!--<link href="bootstrap/css/bootswatch.superhero.css" rel="stylesheet">-->
	<!--<link href="bootstrap/css/bootswatch.journal.css" rel="stylesheet">-->
	<!--<link href="bootstrap/css/bootswatch.slate.css" rel="stylesheet">-->
	<!--<link href="bootstrap/css/bootswatch.spurce.css" rel="stylesheet">-->
	
    <link href="bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap.timepicker.css" rel="stylesheet">
	<link href="bootstrap/css/dataTables.css" rel="stylesheet">
	<link href="bootstrap/css/jqplot.css" rel="stylesheet">
	<link href="docs.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script src="bootstrap/js/base64.IE.min.js"></script>
		<script src="bootstrap/js/json.IE.min.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
	<link rel="shortcut icon" href="favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="bootstrap/favicon_114.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="bootstrap/favicon_72.png">
    <link rel="apple-touch-icon-precomposed" href="bootstarp/favicon_57.png">
  </head>

  <body data-spy="scroll" data-target=".bs-docs-sidebar">
  
	<script>
		var mailer_file_datarunner = '<?=$runner_file;?>';
		var mailer_file_datagetter = '<?=$mailer_file;?>';
		var mailer_file_datasaving = '<?=$saving_file;?>';
		var mailer_file_datasignin = '<?=$mailer_file_datasignin;?>';
		var mailer_file_dataserver = '<?=$mailer_file_dataserver;?>';
		var mailer_file_datanaming = '<?=$mailer_file_datanaming;?>';
		var mailer_file_datarespon = '<?=$mailer_file_datarespon;?>';
	</script>
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="#"><img src="bootstrap/favicon_25.png" style="margin:-6px 6px 0 0">MailBox Responder </a>
				<div class="nav-collapse collapse">
					<ul class="nav pull-right">
					  <li style="display:<?=$login?'':'none';?>" class="signout"><a href="?logout">Sign out</a></li>
					</ul>
				</div>
			</div>
		</div>
    </div><!-- /navbar -->
	
    <div class="container">
		
		<form class="form-signin" style="display:<?=!$login?'':'none';?>" method="post">
			<h2 class="form-signin-heading">Authorized Only</h2>
			<input name="username" type="text" class="input-block-level" placeholder="Username" value="<?=isset($_COOKIE['ui-username'])?$_COOKIE['ui-username']:'';?>">
			<input name="password" type="password" class="input-block-level" placeholder="Password" value="<?=isset($_COOKIE['ui-password'])?mcrypt_decode($_COOKIE['ui-password']):'';?>">
			<div class="row-fluid" style="<?=isset($wrongpwd)&&$wrongpwd?'':'display:none;';?>">
				<div class="alert alert-danger fade in">
					<button type="button" class="close" data-dismiss="alert"><i class="icon-remove-sign" style="margin-top:6px"></i></button>
					Invalid username/password
				</div>
			</div>
			<div class="checkbox pull-right" style="margin-right: 10px">
				<input name="remember" type="checkbox" <?=isset($_COOKIE['ui-password'])?'checked="checked"':'';?> value="remember-me"> Remember my password
			</div>
			<button class="btn btn-primary" name="login" type="submit">Sign in</button>
		</form>
		
		<div class="container-fluid" style="display:<?=$login?'':'none';?>">
			<div class="row-fluid">
			  
				<div class="span3">
					<div class="row" >
						<div class="bs-docs-sidebar">
							<ul class="nav nav-list bs-docs-sidenav">
								<li>
									<a href="#divinbox"><i class="icon-chevron-right"></i> Inbox Message</a>
								</li>
								<li>
									<a href="#divignore"><i class="icon-chevron-right"></i> Ignored Message</a>
								</li>
								<li>
									<a href="#divreply"><i class="icon-chevron-right"></i> Recorded Message</a>
								</li>
								<li>
									<a href="#divsent"><i class="icon-chevron-right"></i> Returned Message</a>
								</li>
								<li>
									<a href="#divfail"><i class="icon-chevron-right"></i> Failed Message</a>
								</li>
								<li>
									<a href="#divconfig"><i class="icon-chevron-right"></i> Responder Config</a>
								</li>
								<li>
									<a onClick="manual_exec(this)" style="cursor:default">Script Last Run: <span id="lastexec"></span></a>
								</li>
							</ul>
						</div><!-- /bs-docs-sidebar -->
					</div><!-- /row -->  
				</div><!--/span-->
				
				<div class="span9 container">
				
					<div class="page-header">
						<h1>MailBox Responder UI</h1>
					</div><!-- /header -->
				<!-- #divinbox -->
					<div id="divinbox" class="navbar">
						<div class="navbar-inner">
							<span class="brand">Incoming Inbox Email</span>
						</div>
					</div><!-- /navbar -->
					<div class="row-fluid">
						<div id="inbox_chart" class="span12" style="height:300px; display:none"></div>
						<div class="span12">	
							<table id="inbox" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>#</th>
									<th>Recipient</th>
									<th>Date</th>
									<th>Subject</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="4">
										<center>
										<div class="btn-group btn-inbox">
											<button class="btn btn-danger  tbl-del" onClick="emptytable('inbox')">Delete Mail</button>
											<button class="btn btn-warning tbl-fet" onClick="inittable('inbox')">Fetch Mail</button>
										</div>
										</center>
									</td>
								</tr>
							</tbody>
							</table>
						</div><!-- /span -->
					</div><!-- /row-fluid -->
					<!-- /#divinbox -->
					
				<!-- #divignore -->
					<div id="divignore" class="navbar">
						<div class="navbar-inner">
							<span class="brand">Incoming Ignored Email</span>
						</div>
					</div><!-- /navbar -->
					<div class="row-fluid">
						<div id="autoignore_chart" class="span12" style="height:300px; display:none"></div>
						<div class="span12">	
							<table id="autoignore" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>#</th>
									<th>Recipient</th>
									<th>Date</th>
									<th>Subject</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="4">
										<center>
										<div class="btn-group btn-autoignore">
											<button class="btn btn-danger  tbl-del" onClick="emptytable('autoignore')">Delete Mail</button>
											<button class="btn btn-warning tbl-fet" onClick="inittable('autoignore')">Fetch Mail</button>
										</div>
										</center>
									</td>
								</tr>
							</tbody>
							</table>
						</div><!-- /span -->
					</div><!-- /row-fluid -->
					<!-- /#divignore -->
				
				<!-- #divreply -->
					<div id="divreply" class="navbar">
						<div class="navbar-inner">
							<span class="brand">Incoming Responded Email</span>
						</div>
					</div><!-- /navbar -->
					<div class="row-fluid">
						<div id="autoreply_chart" class="span12" style="height:300px; display:none"></div>
						<div class="span12">	
							<table id="autoreply" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>#</th>
									<th>Sender</th>
									<th>Date</th>
									<th>Subject</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="4">
										<center>
										<div class="btn-group btn-autoreply">
											<button class="btn btn-danger  tbl-del" onClick="emptytable('autoreply')">Delete Mail</button>
											<button class="btn btn-warning tbl-fet" onClick="inittable('autoreply')">Fetch Mail</button>
										</div>
										</center>
									</td>
								</tr>
								<tr class="sample-content" style="display:none">
									<td>1</td>
									<td>Tester</td>
									<td>Nov 11, 2012 at 12:21 PM</td>
									<td>something
										<button class="btn pull-right" data-email="test@web.com" data-value="dGVzdDEyMw==" 
											onClick="subrowshow(this,'autoreply',0)">Read &raquo;</button>
									</td>
								</tr>
							</tbody>
							</table>
						</div><!-- /span -->
					</div><!-- /row-fluid -->
					<!-- /#divreply -->
					
				<!-- #divsent -->
					<div id="divsent" class="navbar">
						<div class="navbar-inner">
							<span class="brand">Outgoing Responded Email</span>
						</div>
					</div><!-- /navbar -->
					<div class="row-fluid">
						<div id="autosent_chart" class="span12" style="height:300px; display:none"></div>
						<div class="span12">	
							<table id="autosent" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>#</th>
									<th>Recipient</th>
									<th>Date</th>
									<th>Subject</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="4">
										<center>
										<div class="btn-group btn-autosent">
											<button class="btn btn-danger  tbl-del" onClick="emptytable('autosent')">Delete Mail</button>
											<button class="btn btn-warning tbl-fet" onClick="inittable('autosent')">Fetch Mail</button>
										</div>
										</center>
									</td>
								</tr>
							</tbody>
							</table>
						</div><!-- /span -->
					</div><!-- /row-fluid -->
					<!-- /#divsent -->
					
				<!-- #divsenfail -->
					<div id="divfail" class="navbar">
						<div class="navbar-inner">
							<span class="brand">Failed Responded Email</span>
						</div>
					</div><!-- /navbar -->
					<div class="row-fluid">
						<div id="autosent_chart" class="span12" style="height:300px; display:none"></div>
						<div class="span12">	
							<table id="autosenfail" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>#</th>
									<th>Recipient</th>
									<th>Date</th>
									<th>Subject</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="4">
										<center>
										<div class="btn-group btn-autosenfail">
											<button class="btn btn-danger  tbl-del" onClick="emptytable('autosenfail')">Delete Mail</button>
											<button class="btn btn-warning tbl-fet" onClick="inittable('autosenfail')">Fetch Mail</button>
										</div>
										</center>
									</td>
								</tr>
							</tbody>
							</table>
						</div><!-- /span -->
					</div><!-- /row-fluid -->
					<!-- /#divsenfail -->
					
				<!-- #divconfig -->
					<div id="divconfig" class="navbar">
						<div class="navbar-inner">
							<span class="brand">Autorespond Setting</span>
						</div>
					</div><!-- /navbar -->
				<!-- $server -->
					<h4 style="padding-left:25px">Change Mail Configuration</h4>
					<div class="row-fluid">
						<?php
							unset($jsondata);
							$serv_naming = $saving_path.file_naming($mailer_file_dataserver);
							if (file_exists($serv_naming)) {
								$fpointer = fopen($serv_naming, "r");
								$jsondata = fread($fpointer, filesize($serv_naming));
								$jsondata = json_decode(mcrypt_decode($jsondata), true);
								fclose($fpointer);
								if ($jsondata===null) unset($jsondata);
							}
							$jsondata = isset($jsondata)? $jsondata : json_decode($mailer_json_defaultauth,true);
							foreach ($jsondata as $key=>$val) { $jsondata[$key] = str_replace('"','&quot;',$val); }
						?>
						<div class="span12">
							<table class="mailserver-data table table-condensed">
							<tbody>
								<tr>
									<td><center>Incoming Server</center></td>
									<td><center>Incoming Port</center></td>
									<td><center>Incoming Username</center></td>
									<td><center>Incoming Password</center></td>
								</tr>
								<tr>
									<td style="border-top:0"><input class="span12" value="<?=$jsondata['incoming_serv'];?>" servkeys="incoming_serv" placeholder="mail.your.host" type="text"></td>
									<td style="border-top:0"><input class="span12" value="<?=$jsondata['incoming_port'];?>" servkeys="incoming_port" placeholder="143" type="text"></td>
									<td style="border-top:0"><input class="span12" value="<?=$jsondata['incoming_user'];?>" servkeys="incoming_user" placeholder="your@incoming.mail" type="text"></td>
									<td style="border-top:0"><input class="span12" value="<?=$jsondata['incoming_pass'];?>" servkeys="incoming_pass" placeholder="your incoming password" type="password"></td>
								</tr>
								<tr>
									<td><center>Outgoing Server</center></td>
									<td><center>Outgoing Port</center></td>
									<td><center>Outgoing Username</center></td>
									<td><center>Outgoing Password</center></td>
								</tr>
								<tr>
									<td style="border-top:0"><input class="span12" value="<?=$jsondata['outgoing_serv'];?>" servkeys="outgoing_serv" placeholder="mail.your.host" type="text"></td>
									<td style="border-top:0"><input class="span12" value="<?=$jsondata['outgoing_port'];?>" servkeys="outgoing_port" placeholder="25" type="text"></td>
									<td style="border-top:0"><input class="span12" value="<?=$jsondata['outgoing_user'];?>" servkeys="outgoing_user" placeholder="your@outgoing.mail" type="text"></td>
									<td style="border-top:0"><input class="span12" value="<?=$jsondata['outgoing_pass'];?>" servkeys="outgoing_pass" placeholder="your outgoing password" type="password"></td>
								</tr>
								<tr>
									<td <?=!file_exists('../mailgun')?'colspan="2"':'';?> style=""><center>Outgoing Test-To Email</center></td>
									<td style="<?=!file_exists('../mailgun')?'display:none;':'';?>"><center>Outgoing Method</center></td>
									<td><center>Repy-To Email</center></td>
									<td><center>Repy-To Name</center></td>
								</tr>
								<tr>
									<td <?=!file_exists('../mailgun')?'colspan="2"':'';?> style="border-top:0"><input class="span12" value="<?=$jsondata['outgoing_test'];?>" servkeys="outgoing_test" placeholder="your@test-address.mail" type="text"></td>
									<td style="<?=!file_exists('../mailgun')?'display:none;':'';?> border-top:0">
										<select servkeys="outgoing_type" class="span12">
											<option <?=strtolower($jsondata['outgoing_type'])=='default'?'selected="selected"':'';?> > Default </option>
											<option <?=strtolower($jsondata['outgoing_type'])=='mailgun'?'selected="selected"':'';?> > Mailgun </option>
										</select>
									</td>
									<td style="border-top:0"><input class="span12" value="<?=$jsondata['reply_to_mail'];?>" servkeys="reply_to_mail" placeholder="replyto@incoming.mail" type="text"></td>
									<td style="border-top:0"><input class="span12" value="<?=$jsondata['reply_to_name'];?>" servkeys="reply_to_name" placeholder="Reply-To Mail Address" type="text"></td>
								</tr>
								<tr>
									<td style="border-top:0;padding:0 !important" colspan="4">
										<div class="mailserver-act">
											<div class="span4 pull-left">
												<?php
													$nspan = file_exists('../mail_exec.txt')? 4:6;
												?>
												<button class="span<?=$nspan;?> btn btn-default" type="button" task="Incoming" onClick="testconserver(this)">Test Incoming</button>
												<button class="span<?=$nspan;?> btn btn-default" type="button" task="Outgoing" onClick="testconserver(this)">Test Outgoing</button>
												<a class="span<?=$nspan;?> btn btn-default" style="<?=$nspan==6?'display:none;':'';?>" href="../mail_exec.txt" target="_blank">Check Send Log</a>
											</div>
											<div class="span2 pull-right">
												<button class="span12 btn btn-success" type="button" onClick="savemailserver(this)">Save Change</button>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
							</table>
						</div>
					</div><!-- /row-fluid -->
					<!-- /$server -->
				<!-- $attribute -->
					<h4 style="padding-left:25px">Change Attribute Variable</h4>
					<div class="row-fluid">
						<?php
							unset($jsondata);
							$attr_naming = $saving_path.file_naming($mailer_file_datanaming);
							if (!file_exists($attr_naming)) {
								$fpointer = fopen($attr_naming, "w");
								fwrite($fpointer, mcrypt_encode($mailer_json_defaultattr));
								fclose($fpointer);
								$jsondata = json_decode($mailer_json_defaultattr, true);
							} else {
								$fpointer = fopen($attr_naming, "r");
								$jsondata = fread($fpointer, filesize($attr_naming));
								$jsondata = json_decode(mcrypt_decode($jsondata), true);
								fclose($fpointer);
								if ($jsondata===null) unset($jsondata);
							}
							$jsondata = isset($jsondata)? $jsondata : json_decode($mailer_json_defaultattr,true);
							foreach ($jsondata as $key=>$val) { $jsondata[$key] = str_replace('"','\"',$val); }
						?>
						<div class="span12">
							<table class="attribute-data table table-condensed">
							<tbody>
								<tr>
									<td style="border-top:0"><input class="span12" value="<?=$jsondata['$$mail_addr$$'];?>" placeholder="$$mail_addr$$" type="text" onFocus="$(this).popover('show')" data-trigger="focus" data-placement="right" data-animation="true" data-html="true" data-content="Sender:<p style='padding-left:10px'>Best Man Alive (best_man@mail.host)</p><br/>Sample:<br/><p style='padding-left:10px'>Hello &quot;$$mail_addr$$&quot;.</p>Result:<br/><p style='padding-left:10px'>Hello &quot;best_man@mail.host&quot;.</p>" data-original-title="Sender Email Address"></td>
									<td style="border-top:0"><input class="span12" value="<?=$jsondata['$$fron_name$$'];?>" placeholder="$$fron_name$$" type="text" onFocus="$(this).popover('show')" data-trigger="focus" data-placement="top"   data-animation="true" data-html="true" data-content="Sender:<p style='padding-left:10px'>Best Man Alive (best_man@mail.host)</p><br/>Sample:<br/><p style='padding-left:10px'>Hello &quot;$$fron_name$$&quot;.</p>Result:<br/><p style='padding:0 0 20px 10px;border-bottom:solid 1px #DDD'>Hello &quot;Best&quot;.</p><b>ps:</b> if name not exists it will use email name (best_man)" data-original-title="Sender First Name"></td>
									<td style="border-top:0"><input class="span12" value="<?=$jsondata['$$last_name$$'];?>" placeholder="$$last_name$$" type="text" onFocus="$(this).popover('show')" data-trigger="focus" data-placement="top"   data-animation="true" data-html="true" data-content="Sender:<p style='padding-left:10px'>Best Man Alive (best_man@mail.host)</p><br/>Sample:<br/><p style='padding-left:10px'>Hello &quot;$$last_name$$&quot;.</p>Result:<br/><p style='padding:0 0 20px 10px;border-bottom:solid 1px #DDD'>Hello &quot;Man Alive&quot;.</p><b>ps:</b> if name not exists it will use email name (best_man)" data-original-title="Sender Last Name"></td>
									<td style="border-top:0"><input class="span12" value="<?=$jsondata['$$full_name$$'];?>" placeholder="$$full_name$$" type="text" onFocus="$(this).popover('show')" data-trigger="focus" data-placement="left"  data-animation="true" data-html="true" data-content="Sender:<p style='padding-left:10px'>Best Man Alive (best_man@mail.host)</p><br/>Sample:<br/><p style='padding-left:10px'>Hello &quot;$$full_name$$&quot;.</p>Result:<br/><p style='padding:0 0 20px 10px;border-bottom:solid 1px #DDD'>Hello &quot;Best Man Alive&quot;.</p><b>ps:</b> if name not exists it will use email name (best_man)" data-original-title="Sender Full Name"></td>
								</tr>
								<tr>
									<td style="border-top:0"><input class="span12" value="<?=$jsondata['$$mail_subj$$'];?>" placeholder="$$mail_subj$$" type="text" onFocus="$(this).popover('show')" data-trigger="focus" data-placement="right" data-animation="true" data-html="true" data-content="Sample:<br/><p style='padding-left:10px'>We receive your mail about &quot;$$mail_subj$$&quot;.</p>Result:<br/><p style='padding-left:10px'>We receive your mail about &quot;This is some subject&quot;.</p>" data-original-title="Sender Mail Subject"></td>
									<td style="border-top:0"><input class="span12" value="<?=$jsondata['$$mail_quot$$'];?>" placeholder="$$mail_quot$$" type="text" onFocus="$(this).popover('show')" data-trigger="focus" data-placement="top"   data-animation="true" data-html="true" data-content="Sample:<br/><p style='padding-left:10px'>Thankyou for mailing me. $$mail_quot$$</p>Result:<br/><p style='padding-left:10px'>Thankyou for mailing me. <br/><br/>On Sun, Jan 1, 2012 at 7:30 AM, Best Man wrote:<br/>><br/>> Hi, it's me your best man.<br/>><br/>><br/></p>" data-original-title="Sender Quoted Message"></td>
									<td style="border-top:0"><input class="span12" value="<?=$jsondata['$$mail_date$$'];?>" placeholder="$$mail_date$$" type="text" onFocus="$(this).popover('show')" data-trigger="focus" data-placement="top"   data-animation="true" data-html="true" data-content="Sample:<br/><p style='padding-left:10px'>We receive your mail at &quot;$$mail_date$$&quot;.</p>Result:<br/><p style='padding-left:10px'>We receive your mail at &quot;15/Jan/2012 10:00AM&quot;.</p>" data-original-title="Sender Incoming Date"></td>
									<td style="border-top:0"><input class="span12" value="<?=$jsondata['$$sent_date$$'];?>" placeholder="$$sent_date$$" type="text" onFocus="$(this).popover('show')" data-trigger="focus" data-placement="left"  data-animation="true" data-html="true" data-content="Sample:<br/><p style='padding-left:10px'>Today at &quot;$$sent_date$$&quot; we sent you email.</p>Result:<br/><p style='padding-left:10px'>Today at &quot;1/Nov/2012, 1:15PM&quot; we sent you email.</p>" data-original-title="Reply Sent Date"></td>
								</tr>
								<tr>
									<td style="border-top:0;padding:0 !important" colspan="4">
										<button class="attribute-act span2 btn btn-success pull-right" type="button" onClick="saveattributelist()">Save Change</button>
										<button type="text" class="span9 btn" onClick="$('#triggerdetail').modal()">Text checker (click for details): &lt;trigger&gt;&lt;/trigger&gt;</button>
									</td>
								</tr>
							</tbody>
							</table>
						</div>
					</div><!-- /row-fluid -->
					<!-- /$attribute -->
				<!-- $attachment -->
					<div class="row-fluid">
						<div class="span10">
						<table class="table table-condensed">
							<tr>
								<th style="border-top:0;padding:0 !important">
									<center>Attribute Variable For Attachment</center>
								</th>
							</tr>
					<?php
						$files = array();
						$dir = opendir($saving_path.'attachment');
						while ($files[] = readdir($dir));
						closedir($dir);
						foreach ($files as $attachment)
						if ($attachment != '..' && $attachment != '.' && $attachment != '') {?>
							<tr>
								<td>
									<center>
										<input type="text" class="span10" value="&lt;attachment&gt;<?=$attachment;?>&lt;/attachment&gt;" onClick="$(this).select()" readonly="true" style="text-align:center; cursor:auto !important;">
										<button class="span1 btn btn-danger pull-right" onClick="removeattachment(this,'<?=$attachment;?>')" type="button"><i class="icon-remove-sign"></i></button>
									</center>
								</td>
							</tr><?
						}?>
						</table>
						</div>
						<div class="span2" style="margin-top:25px">
							<button id="uploadattachment" class="btn btn-success span12" type="button">Add New File</button>
						</div>
					</div><!-- /row-fluid -->
					<!-- /$template -->
				<!-- $template -->
					<h4 style="padding-left:25px">Change Response Message</h4>
					<div style="border-top: 1px solid #DDD; margin-bottom: 10px"></div>
					<div class="row-fluid">
						<?php
							unset($jsondata);
							$responds_naming = $saving_path.file_naming($mailer_file_datarespon);
							$respondoption[] = '<option>inactive</option>';
							if (file_exists($responds_naming)) {	
								$fpointer = fopen($responds_naming, "r");
								$jsondata = fread($fpointer, filesize($responds_naming));
								$jsondata = json_decode(mcrypt_decode($jsondata), true);
								fclose($fpointer);
								if ($jsondata!==null) {
									for ($i=1; $i<=$jsondata['count']; $i++) $respondoption[] = "<option>{$i}</option>";
								} else { unset($jsondata); }
							}
							$respondform_dny = isset($jsondata['black'])? $jsondata['black'] : '';
							$respondform_dly = isset($jsondata['delay'])? $jsondata['delay'] : '00:01:00';
							$respondform_max = isset($jsondata['limit'])? $jsondata['limit'] : '1';
							$respondform_use = isset($jsondata['using'])? $jsondata['using'] : true;
						?>
						<div class="respond-use span12" style="<?=$respondform_use?'display:none;':'';?>">
							<div class="alert">
								MailBox Responder curently disabled, click checkbox bellow to activate.
							</div>
						</div>
						<span class="respond-use respond-list" style="<?=$respondform_use?'':'display:none;';?>">
						<?php
							$respondformdata = isset($jsondata['count'])? $jsondata['count'] : 0;
							for ($i=-1; $i<$respondformdata; $i++) {
								if ($i>=0) {
									$respondformcss = '';
									$respondform_no = $jsondata[$i]['no'];
									$respondform_t1 = $jsondata[$i]['t1'];
									$respondform_t2 = $jsondata[$i]['t2'];
									$respondform_tx = $jsondata[$i]['tx'];
								} else {
									$respondformcss = 'display:none;';
									$respondform_no = 'inactive';
									$respondform_t1 = '00:01:00';
									$respondform_t2 = '23:59:00';
									$respondform_tx = '';
								}?>
								<div class="respond-data row-fluid" style="<?=$respondformcss;?>">
									<div class="span3">
										<div class="row-fluid input-append input-prepend">
											<span class="add-on" style="width:20%; text-align:left">Reply</span>
											<select class="respond-order" style="width:50%">
												<?=str_replace('>'.$respondform_no,' selected >'.$respondform_no,implode('',$respondoption));?>
											</select>
											<button class="btn btn-danger" onClick="delrespondlist(this)" style="width:25%;"><i class="icon-remove-sign"></i></button>
										</div>
										<div class="row-fluid input-prepend">
											<span class="add-on" style="width:20%; text-align:left">From</span>
											<span style="width:65%;">
												<input class="timepicker" type="text" value="<?=$respondform_t1;?>" style="width:inherit">
												<i class="icon-time" style="margin: -2px 0 0 -22.5px; pointer-events: none; position: relative; z-index:10"></i>
											</span>
										</div>
										<div class="row-fluid input-prepend">
											<span class="add-on" style="width:20%; text-align:left">Until</span>
											<span style="width:65%;">
												<input class="timepicker" type="text" value="<?=$respondform_t2;?>" style="width:inherit">
												<i class="icon-time" style="margin: -2px 0 0 -22.5px; pointer-events: none; position: relative; z-index:10"></i>
											</span>
										</div>
									</div>
									<div class="span9">
										<textarea class="span12" placeholder="Message template" wrap="off" style="height:100px"><?=$respondform_tx;?></textarea>
									</div>
								</div><?
							}
						?>
						</span><!-- /respond-list -->
						<div class="respond-act ">
							<div class="row-fluid">
								<div class="span3">
									<div class="row-fluid input-prepend">
										<span class="add-on" style="width:85%; border-right:0; text-align:left">Black List Email &nbsp;&raquo;&raquo;</span>
										<span style="width:0%;"><input type="text" disabled style="width:inherit; border-left:0; background-color:#EEE; cursor:default"></span>
									</div>
								</div>
								<div class="span9">
									<input type="text" class="span12 blacklist" value="<?=$respondform_dny;?>" placeholder="address_fullname, address_somename, address_fullname@mailserver.host, mailserver.host">
								</div>
							</div>
							<div class="row-fluid">
								<div class="span3 checkbox" style="margin-top:6px">
									<input type="checkbox" <?=$respondform_use?'checked':'';?> onChange="$('.respond-use').toggle();menupos();" style="margin:4px 10px 10px -15px"> Enable auto response
								</div>
								<div class="span3 input-prepend">
									<span class="add-on" style="width:50%; text-align:left">Sending Delay</span>
									<span style="width:40%;">
										<input class="timepicker" type="text" value="<?=$respondform_dly;?>" style="width:inherit">
										<i class="icon-time" style="margin: -2px 0 0 -22.5px; pointer-events: none; position: relative; z-index:10"></i>
									</span>
								</div>
								<div class="span2 input-prepend">
									<span class="add-on" style="width:60%; text-align:left">Max Each</span>
									<span style="width:30%;">
										<input class="maxlimit" type="number" value="<?=is_numeric($respondform_max)?$respondform_max:1;?>" style="width:inherit">
										<i class="icon-signal" style="margin: -2px 0 0 -22.5px; pointer-events: none; position: relative; z-index:10"></i>
									</span>
								</div>
								<button class="span2 btn respond-use" type="button" onClick="addrespondlist()">Add Response</button>
								<button class="span2 btn btn-success pull-right" type="button" onClick="saverespondlist()">Save Change</button>
							</div>
						</div>
					</div><!-- /row-fluid -->
					<!-- /$template -->
				<!-- $loginauth -->
					<h4 style="padding-left:25px">Change Login Details</h4>
					<div style="border-top: 1px solid #DDD; margin-bottom: 10px"></div>
					<div class="signin-data row-fluid">
						<div class="span4">
							<input class="span12" id="newusername" placeholder="New Username" type="text" value="<?=isset($_COOKIE['ui-username'])?$_COOKIE['ui-username']:'';?>">
						</div>
						<div class="span4">
							<input class="span12" id="newpassword" placeholder="New Password" type="text">
						</div>
						<div class="span4">
							<input class="span12" id="oldpassword" placeholder="Old Password" type="password" onFocus="$(this).popover('hide')" data-trigger="manual" data-placement="left" data-animation="true" data-content="You have entered incorrect data" data-original-title="Inccorect Password">
						</div>
						<div class="signin-act row-fluid">
							<button class="span2 btn btn-success pull-right" type="button" onClick="savesignindata()">Save Change</button>
						</div>
					</div><!-- /row-fluid -->
					<!-- /$loginauth -->
				</div><!-- /span -->
				<!-- /#divconfig -->
				<!-- trigger -->
				<div id="triggerdetail" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-header">
						<button class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-remove-sign" style="margin:5px"></i></button>
						<h3 id="myModalLabel">Trigger Variable</h3>
					</div>
					<div class="modal-body">
						<p>
							using with:<br/>
							&lt;trigger&gt;<br/>
							&nbsp;&nbsp;&nbsp;&lt;check&gt;text-to-find-on-incoming-mail&lt;/check&gt;<br/>
							&nbsp;&nbsp;&nbsp;&lt;found&gt;random-answer-if-text-found-01&lt;/found&gt;<br/>
							&nbsp;&nbsp;&nbsp;&lt;found&gt;random-answer-if-text-found-02&lt;/found&gt;<br/>
							&nbsp;&nbsp;&nbsp;&lt;found&gt;random-answer-if-text-found-..&lt;/found&gt;<br/>
							&nbsp;&nbsp;&nbsp;&lt;empty&gt;random-answer-if-not-found-01&lt;/empty&gt;<br/>
							&nbsp;&nbsp;&nbsp;&lt;empty&gt;random-answer-if-not-found-02&lt;/empty&gt;<br/>
							&nbsp;&nbsp;&nbsp;&lt;empty&gt;random-answer-if-not-found-..&lt;/empty&gt;<br/>
							&lt;/trigger&gt;<br/>
							<br/>
							rules:<br/>
							&lt;check&gt;&lt;/check&gt; is look-up for text in incoming message<br/>
							&lt;check&gt;&lt;/check&gt; cannot use attribute-variable<br/>
							&lt;check&gt;&lt;/check&gt; if empty will direcly use &lt;found&gt;<br/>
							&lt;found&gt;&lt;/found&gt; is answer if look-up match<br/>
							&lt;found&gt;&lt;/found&gt; can use attribute-variable<br/>
							&lt;found&gt;&lt;/found&gt; if it more than one, it will be randomly<br/>
							&lt;empty&gt;&lt;/empty&gt; is answer if look-up not-match<br/>
							&lt;empty&gt;&lt;/empty&gt; can use attribute-variable<br/>
							&lt;empty&gt;&lt;/empty&gt; if it more than one, it will be randomly<br/>
							&lt;empty&gt;&lt;/empty&gt; can being empty or not defined at all<br/>
							<br/>
							example:<br/>
							&lt;trigger&gt;<br/>
							&nbsp;&nbsp;&nbsp;&lt;check&gt;what are you doing&lt;/check&gt;<br/>
							&nbsp;&nbsp;&nbsp;&lt;found&gt;Iam curently replying you $$fron_name$$&lt;/found&gt;<br/>
							&nbsp;&nbsp;&nbsp;&lt;found&gt;Iam watching television&lt;/found&gt;<br/>
							&nbsp;&nbsp;&nbsp;&lt;empty&gt;Do you know what iam doing now?&lt;/empty&gt;<br/>
							&lt;/trigger&gt;<br/>
							<br/>
							respond-template:<br/>
							Hay, &lt;trigger&gt;&lt;check&gt;what are you doing&lt;/check&gt;&lt;found&gt;Iam curently replying you $$fron_name$$&lt;/found&gt;&lt;found&gt;Iam watching television&lt;/found&gt;&lt;empty&gt;Do you know what iam doing now?&lt;/empty&gt;&lt;/trigger&gt;.<br/>
							what you doing now?<br/>
							<br/>
							incoming-message:<br/>
							from "Jhon Doe" <jhon.doe@mail.com><br/>
							hey what are you doing?<br/>
							<br/>
							posiible-respond:<br/>
							Hay, Iam curently replying you Jhon.<br/>
							what you doing now?<br/>
							<br/>
							posiible-respond:<br/>
							Hay, Iam watching television.<br/>
							what you doing now?<br/>
							<br/>
							incoming-message:<br/>
							from "Jhon Doe" <jhon.doe@mail.com><br/>
							hey where are you now?<br/>
							<br/>
							posiible-respond:<br/>
							Hay, Do you know what iam doing now?.<br/>
							what you doing now?
						</p>
					</div>
					<div class="modal-footer">
						<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
					</div>
				</div>
				<!-- /trigger -->
				
			</div><!-- /row-fluid -->
		</div><!-- /container-fluid -->
			
    </div> <!-- /container -->

	<hr/>

	<footer>
		<center class="container">
			<p>&copy; <a href="mailto:sumonbdinfo@gmail.com">Sumon Khan</a> 2014</p>
		</center>
	</footer>

  </body>
</html>
