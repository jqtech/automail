<?php

	set_time_limit(0);
	
	$croncalls = true;
	include 'mail.php';
	
	$delay = isset($argv[1]) && is_numeric($argv[1])? $argv[1] : 1;
	$kills = isset($argv[2]) && is_numeric($argv[2])? $argv[2] : 60;

	//execInBackground('php '.str_replace('cron','ctest',__FILE__));
	function execInBackground($cmd) {
		if (substr(php_uname(), 0, 7) == 'Windows'){
			pclose(popen('start /B '. $cmd, 'r'));
		} else {
			exec($cmd . ' > /dev/null &');
		}
	}
	
	$total_time = 1;
	$start_time = microtime(true)-1;
	while ($total_time < $kills) {
		$cronmailer->send($jsontemplates,$jsonattribute);
		
		$total_time =  (int) (microtime(true) - $start_time);
		sleep($delay);
	}
	
	$cronmailer->close_mailbox();
	