u can setup cron every minute with trying to keep alive script every second for 60second with:
*/1 * * * * php /home4/sumon/public_html/newmailer/cron.php 1 60

or  setup cron every minute with trying to keep alive script every second for 5min (300second) with:
*/5 * * * * php /home4/sumon/public_html/newmailer/cron.php 1 300

or other example, if u want to make the script alive every 5second for 5min , use this:
*/5 * * * * php /home4/sumon/public_html/newmailer/cron.php 5 300