<?php

class autoresponse{
	
	var $inc_server		= '';
	var $inc_port		= '';
	var $inc_type		= '';
	var $inc_username	= '';
	var $inc_password	= '';
	
	var $out_server		= '';
	var $out_port		= '';
	var $out_username	= '';
	var $out_password	= '';
	var $out_type		= '';
	
	var $responseEmail	= '';
	var $responseName	= '';
	var $message		= '';
	
	//var $mbox	= null;
	var $sent	= null;
	
	var $debug  = array();

	function autoresponse($inc_user,$inc_pass,$respondermail='',$respondername='',$inc_server='localhost',$servertype='pop3',$inc_port='110',$out_user='',$out_pass='',$out_server='',$out_port='25',$out_type='default') {
		$this->inc_server		= $inc_server;
		$this->inc_port			= $inc_port;
		$this->inc_type			= $servertype;
		$this->inc_username		= $inc_user;
		$this->inc_password		= $inc_pass;
		$this->responseEmail	= $respondermail==""? $inc_user : $respondermail;
		$this->responseName		= $respondername==""? 'postmaster' : $respondername;
		$this->out_server		= $out_server==""? $inc_server : $out_server;
		$this->out_port			= $out_port==""? $inc_port : $out_port;
		$this->out_username		= $out_user==""? $inc_user : $out_user;
		$this->out_password		= $out_pass==""? $inc_pass : $out_pass;
		$this->out_type			= $out_type==""? 'default' : strtolower($out_type);
	}
	
	function strconn() {
		switch($this->inc_type) {
			case 'imap':
				$conn ='{'.$this->inc_server.':'.($this->inc_port	==''?'143':$this->inc_port	).'}'; // imap mailbox
				break;
			case 'pop':
				$conn ='{'.$this->inc_server.':'.$this->inc_port	.'/pop3}'; // pop mailbox 
				break;
			default:
				$conn ='{'.$this->inc_server.':'.$this->inc_port	.'/'.$this->inc_type.'}';
				break;
		}
		return $conn;
	}
	
	function connect($folder='INBOX') {
		/* connect to the mailbox and make a active link */
		try {
			$connect = imap_open($this->strconn().$folder,$this->inc_username,$this->inc_password,NULL,1);
		} catch (Exception $e) {
			//can't open resource, probably already removed
		}
		if ($connect===false) {
			$reject = false;
			foreach (imap_errors() as $error) {
				if (!$reject) if (stristr($error,'invalid credentials')!==false) $reject = true;
			}
			print 'cannot connecting, please '.($reject? 'open <a href="https://mail.google.com" target="_blank">'.$this->inc_username.'</a> manually first with password: '.$this->inc_password.'':'check configuration').PHP_EOL;
			$this->close_mailbox();
			return $connect;
		}
		$this->mbox = $connect;
	}
	
	function mail($sendto,$subject,$message,$backup=false,$html=true,$debug=false) {
		$return = true;
		$senlog = '';
		if ($this->out_type=='mailgun' && file_exists('mailgun')!=false) {
			/* send using mailgun api */
			//require 'vendor/autoload.php';
			require 'mailgun/Mailgun.php';
			//use mailgun\Mailgun;
			
			$domain = $this->out_username;
			$gunapi = new Mailgun($this->out_password);
			
			$mailgunopt = array();
			$mailgunopt['from'] = "{$this->responseName} <{$this->responseEmail}>";
			$mailgunopt['to'] =  $sendto;
			$mailgunopt['subject'] = 'Re: '.$subject;
			if (is_array($message)) {
				$mailgunopt['attachment'] = array();
				foreach($message['file'] as $file) {
					if (file_exists('attachment/'.$file)) {
						$mailgunopt['attachment'][] = 'attachment/'.$file;
					}
				}
				$mailgunopt['html'] = "\r\n\r\n".$message['body'];
			} else {
				$mailgunopt['html'] = "\r\n\r\n".$message;
			}

			# Make the call to the client.
			$result = $gunapi->sendMessage($domain,$mailgunopt);
			/* end! using mailgun */
		} else {
			/* send using smtp */
			require_once('mail_send.php');
			$sendMailer = new PHPMailer(true);
			$sendMailer->SMTPDebug = 2;
			if (trim($this->out_server)!=='' && trim($this->out_port) != '') {
				$sendMailer->IsSMTP();
				$sendMailer->SMTPAuth = true;
				$sendMailer->Host = $this->out_server;
				$sendMailer->Port = (int)$this->out_port;
				$sendMailer->Username = $this->out_username;
				$sendMailer->Password = $this->out_password;
			}
			$sendMailer->ClearAddresses();
			$sendMailer->AddAddress($sendto);
			$sendMailer->AddReplyTo($this->responseEmail, $this->responseName);
			$sendMailer->From	  = $this->responseEmail;
			$sendMailer->FromName = $this->responseName;
			$sendMailer->Subject  = 'Re: '.$subject;
			$sendMailer->IsHTML($html);
			if (is_array($message)) {
				foreach($message['file'] as $file) {
					if (file_exists('attachment/'.$file)) {
						$sendMailer->AddAttachment('attachment/'.$file);
					}
				}
				$sendMailer->Body = "\r\n\r\n".$message['body'];
			} else {
				$sendMailer->Body = "\r\n\r\n".$message;
			}
			ob_start();
			try {
				$sendMailer->Send();
			} catch (Exception $e) {
				//can't send mail, probably conection rejected
				$return = false;
			}
			$senlog = ob_get_contents();
			ob_end_clean();
			if ($debug) print $senlog;
			/* end! using smtp */
		}
		
		if (!$return) {
			//make a fail copy
			imap_append($this->sent,  $this->strconn().'INBOX.AUTOSENFAIL'
			   , "From: {$sendto}\r\n"
			   . "To: {$this->responseEmail}\r\n"
			   . "Subject: FAIL-LOG: {$subject}\r\n"
			   . "<h3>Sending Log:</h3>{$senlog}\r\n{$sendMailer->Body}"
			);
		} else
		if ($backup) {
			//make a save copy
			imap_append($this->sent,  $this->strconn().'INBOX.AUTOSENT'
			   , "From: {$sendto}\r\n"
			   . "To: {$this->responseEmail}\r\n"
			   . "Subject: AUTOREPLY: {$subject}\r\n"
			   . $sendMailer->Body
			);
		}
		unset($sendMailer);
		return $return;
	}
	
	function paging($criteria,$subject='',$deleteMails=false,$page_req=0,$page_max=10) {
		$data = array();
		$data['chck'] = imap_check($this->mbox);
		
		$page_req = ($page_req < 1) ? 1 : ($page_req + 1);
        $page_max = (($page_max - $page_req) != ($page_max-1)) ? ($page_req + ($page_max-1)) : $page_max;
		$page_max = ($data['chck']->Nmsgs < $page_max) ? $data['chck']->Nmsgs : $page_max;
		$page_ing = range($page_req, $page_max);
		$list = imap_fetch_overview($this->mbox,implode($page_ing,','));
		
		if ($list===false) $list = array();
		
		foreach ($list as $num)
		{
			$processed = true;
			if ($deleteMails) {
				$processed = imap_delete($this->mbox,$num->msgno);
			}
			if ($processed!==false) {
				$email_header = $num;
				if (trim($subject)=='' || stristr($email_header->subject,$subject)==false) {
					$data[$num->msgno]['head'] = $email_header;
					$data[$num->msgno]['body'] = imap_fetchbody($this->mbox,$num->msgno,1);
				}
			}
		}
		
		return $data;
	}
	
	function search($criteria,$subject='',$deleteMails=false) {
		$data = array();
		$list = imap_search($this->mbox,$criteria);
		
		if ($list===false) $list = array();
		
		foreach ($list as $num)
		{
			$processed = true;
			if ($deleteMails) {
				$processed = imap_delete($this->mbox,$num);
			}
			if ($processed!==false) {
				$email_header = imap_headerinfo($this->mbox,$num);
				if (trim($subject)=='' || stristr($email_header->subject,$subject)==false) {
					$data[$num]['head'] = $email_header;
					$data[$num]['body'] = imap_fetchbody($this->mbox,$num,1);
				}
			}
		}
		
		return $data;
	}
	
	function send($template=array(),$attribute=array(),$deleteMails=false,$dontmakeCopy=false) {
		$template_ready = false;
		$template_count = isset($template['count'])? $template['count'] : 0;
		$template_delay = isset($template['delay'])? $template['delay'] : '00:00:00';
		$outgoing_limit = isset($template['limit'])? (is_numeric($template['limit'])?$template['limit']:1) : 1;
		$blacklist = array();
		if (isset($template['black'])) {
			foreach (explode(',',$template['black']) as $denyfrom) {
				if (trim($denyfrom)!=='') $blacklist[] = trim($denyfrom);
			}
		}
		//no template available / reasponder disabled
		if (isset($template['using']) && $template['using']==true && $template_count>0) {
			$template_ready = $this->mbox!==false? true : false;
		}
		if (!$template_ready) {
			print 'curently no template available / responder has been disabled from UI'.PHP_EOL;
		} else {
			unset($template['using']);
			unset($template['count']);
			unset($template['delay']);
			unset($template['limit']);
			unset($template['black']);
			
			//creating folder if not exists
			$autotrash = true;
			if (imap_status($this->mbox, $this->strconn().'INBOX.AUTOIGNORE', SA_ALL)===false)
				$autotrash = imap_createmailbox($this->mbox, $this->strconn().'INBOX.AUTOIGNORE');
			$autoinbox = true;
			if (imap_status($this->mbox, $this->strconn().'INBOX.AUTOREPLY', SA_ALL)===false)
				$autoinbox = imap_createmailbox($this->mbox, $this->strconn().'INBOX.AUTOREPLY');
			$autosenf = true;
			if (imap_status($this->mbox, $this->strconn().'INBOX.AUTOSENFAIL', SA_ALL)===false)
				$autosenf = imap_createmailbox($this->mbox, $this->strconn().'INBOX.AUTOSENFAIL');
			$autosent = true;
			if (imap_status($this->mbox, $this->strconn().'INBOX.AUTOSENT', SA_ALL)===false)
				$autosent = imap_createmailbox($this->mbox, $this->strconn().'INBOX.AUTOSENT');
			if ($autosent !== false) {
				//connecting to save folder
				$this->sent = imap_open($this->strconn().'INBOX.AUTOSENT',$this->inc_username,$this->inc_password,NULL,1);
				if ($this->sent == false) {
					$template_ready = false;
					print 'cannot connecting save folder, please try again'.PHP_EOL;
					//closing save folder
					$this->remove_resource($this->sent);
				}
			}
		}
		if ($template_ready) {
			//reading mail header
			$headers	= imap_headers($this->mbox);
			$mail_id	= array();
			$move_mail	= array();
			$ignr_mail	= array();
			for ($num=1; $num<=count($headers); $num++)
			{
				//checking sending limit
				if (count($mail_id)<=$outgoing_limit) {
					$mail_ready = true;
					$mail_head = imap_headerinfo($this->mbox,$num);
					//if ($mail_head->Flagged=='F') $mail_ready = false;
					$sender	= $mail_head->from[0];
					$sendto = $mail_head->reply_to[0];
					
					$sendto_mail = trim($sender->mailbox.'@'.$sender->host);
					$sendto_mail = trim($sendto->mailbox.'@'.$sendto->host)!=''? $sendto->mailbox.'@'.$sendto->host : $sendto_mail;
					$sendto_mail = trim($sendto_mail);
					
					foreach ($blacklist as $skip) {
						if (stristr($sendto_mail,$skip)) {
							//imap_delete($this->mbox,$num);
							$ignr_mail[] = $num;
							$mail_ready = false;
						}
					}
					$this->debug[$num] = array();
					if ($mail_ready) {
						$mail_unix = $mail_head->udate;
						$sent_date = date("d/M/Y h:iA",time());
						$mail_date = date("d/M/Y h:iA",$mail_unix);
						
						$mail_body = imap_fetchbody($this->mbox,$num,1);
						//$mail_orig = explode('<br />', nl2br($mail_body));
						$mail_orig = str_replace("\r\n","\n",$mail_body);
						$mail_orig = str_replace("\r","\n",$mail_orig);
						$mail_orig = explode("\n",$mail_orig);
						$mail_quot = "\r\n\r\nOn ".date("D, M d, Y")." at ".date("h:i A",$mail_head->udate).", {$sendto->personal} <{$sender->mailbox}@{$sender->host}> wrote:\r\n";
						foreach ($mail_orig as $line) $mail_quot .= ">".(substr(trim($line),0,1)=='>'?"":" ").trim($line)."\r\n";
						
						$sendto_text = '';
						$sendto_titl = trim($mail_head->subject);
						$sendto_titl = trim(str_ireplace('re:','',$sendto_titl));
						$sendto_name =isset($sender->personal)? $sender->personal : $sendto_mail;
						$sendto_name =isset($sendto->personal)? $sendto->personal : $sendto_name;
						$sendto_name = trim($sendto_name);
						$sendto_f_nm = strpos($sendto_name.' ',' ');
						$sendto_f_nm = substr($sendto_name,0,$sendto_f_nm);
						$sendto_f_nm = trim($sendto_f_nm);
						$sendto_l_nm = strpos($sendto_name,' ');
						$sendto_l_nm = substr($sendto_name,$sendto_l_nm,strlen($sendto_name));
						$sendto_l_nm = trim($sendto_l_nm);
						
						//check subject & sender from save folder >> count message
						$templatenum = imap_search($this->sent,'FROM " '.$sendto_mail.' " SUBJECT " '.$sendto_titl.' "');
						if ($templatenum===false) {
							//$mail_ready = false;
							//print 'cannot retrieve mail filter, please try again'.PHP_EOL;
							$templatenum = 1;
						} else {
							$templatenum = count($templatenum) +1;
						}
						//add debug
						$this->debug[$num]['mail_date'  ] = date("d/M/Y h:i:s",$mail_unix);
						$this->debug[$num]['sendto_titl'] = $sendto_titl;
						$this->debug[$num]['sendto_mail'] = $sendto_mail;
						$this->debug[$num]['sendto_name'] = $sendto_name;
						$this->debug[$num]['templatenum'] = $templatenum;
						
						//check mail_date+delay if less then Now() -> fomated as day-min:sec
						$respon_time = $this->time2int( date('H:i:s',$mail_unix), $template_delay, date('d',$mail_unix) );
						$curren_time = $this->time2int( date('H:i:s',  time()  ),   '00:00:00'   , date('d',  time()  ) );
						$mail_ready  = $respon_time<=$curren_time? true : false;
						if (isset($_REQUEST['debug'])) $mail_ready = true;
						
						$this->debug[$num]['incomintime'] = date("H:i:s",$mail_unix  ).' --> '.number_format($mail_unix);
						$this->debug[$num]['delayedtime'] = $template_delay.' --> '.number_format($this->time2int($template_delay));
						$this->debug[$num]['respondtime'] = date('H:i:s',$respon_time).' --> '.number_format($respon_time);
						$this->debug[$num]['currenttime'] = date('H:i:s',$curren_time).' --> '.number_format($curren_time);
						$this->debug[$num]['template'   ] = array();
					}
					if ($mail_ready) {
						//check is template-number available
						for ($ti=0; $ti<$template_count; $ti++) {
							$template_reply = isset($template[$ti]['no'])? trim($template[$ti]['no']) : '';
							$template_time1 = isset($template[$ti]['t1'])? trim($template[$ti]['t1']) : '';
							$template_time2 = $this->time2int('now');
							$template_time3 = isset($template[$ti]['t2'])? trim($template[$ti]['t2']) : '';
							$template_value = isset($template[$ti]['tx'])? trim($template[$ti]['tx']) : '';
							
							if ($template_reply==$templatenum && $template_time1!=='' && $template_time3!=='' && $template_value!=='') {
								//check if Now() is between reply time
								$template_time1 = $tmptimef = $this->time2int($template_time1);
								$template_time3 = $tmptimeu = $this->time2int($template_time3);
								if ($template_time1 > $template_time3) {
									$template_time1 = $tmptimeu;
									$template_time3 = $tmptimef;
								}
								if ($template_time1< $template_time2 && $template_time2 < $template_time3) {
									$sendto_text = $template_value;
								}
								//add debug
								$this->debug[$num]['template'][$ti] = array();
								$this->debug[$num]['template'][$ti]['no'] = $template_reply;
								$this->debug[$num]['template'][$ti]['t1'] = $template_time1;
								$this->debug[$num]['template'][$ti]['t2'] = $template_time2;
								$this->debug[$num]['template'][$ti]['t3'] = $template_time3;
							}
						}
						if (trim($sendto_text)=='') $mail_ready = false;
					}
					if ($mail_ready) {
						//convert attachment parameter
						$this->debug[$num]['attachment'] = array();
						$sendto_file = array();
						preg_match_all('{'.preg_quote('<attachment>').'(.*?)'.preg_quote('</attachment>').'}s', $sendto_text, $intextfileattr, PREG_PATTERN_ORDER);
						for ($i=0; $i<count($intextfileattr[0]); $i++) {
							$sendto_file[] = trim($intextfileattr[1][$i]);
							$this->debug[$num]['attachment'][] = $intextfileattr[1][$i];
							//remove attachment-parameter from mail message
							$sendto_text = str_replace('<attachment>'.$intextfileattr[1][$i].'</attachment>','',$sendto_text);
						}
						unset($intextfileattr);
						//convert trigger parameter
						$this->debug[$num]['trigger'] = array();
						preg_match_all('{'.preg_quote('<trigger>').'(.*?)'.preg_quote('</trigger>').'}s', $sendto_text, $intextfileattr, PREG_PATTERN_ORDER);
						for ($i=0; $i<count($intextfileattr[0]); $i++) {
							$this->debug[$num]['trigger'][$i] = array();
							//search & random found-attribute inside each trigger
							$apply_trigger_text = '';
							preg_match_all('{'.preg_quote('<found>').'(.*?)'.preg_quote('</found>').'}s', $intextfileattr[1][$i], $attr_trigger, PREG_PATTERN_ORDER);
							if (count($attr_trigger[0])>0) {
								$apply_trigger_text = rand(0, count($attr_trigger[0])-1);
								$apply_trigger_text = trim($attr_trigger[1][$apply_trigger_text]);
							}
							$this->debug[$num]['trigger'][$i]['found'] = $apply_trigger_text;
							unset($attr_trigger);
							//search & random empty-attribute inside each trigger
							$empty_trigger_rand = '';
							preg_match_all('{'.preg_quote('<empty>').'(.*?)'.preg_quote('</empty>').'}s', $intextfileattr[1][$i], $attr_trigger, PREG_PATTERN_ORDER);
							if (count($attr_trigger[0])>0) {
								$empty_trigger_rand = rand(0, count($attr_trigger[0])-1);
								$empty_trigger_rand = trim($attr_trigger[1][$empty_trigger_rand]);
							}
							$this->debug[$num]['trigger'][$i]['empty'] = $empty_trigger_rand;
							unset($attr_trigger);
							//search look-up text for this trigger and check if found on
							preg_match_all('{'.preg_quote('<check>').'(.*?)'.preg_quote('</check>').'}s', $intextfileattr[1][$i], $attr_trigger, PREG_PATTERN_ORDER);
							if (count($attr_trigger[0])>0) {
								$apply_trigger_text = stristr($mail_body,trim($attr_trigger[1][0]))===false? $empty_trigger_rand : $apply_trigger_text;
								$this->debug[$num]['trigger'][$i]['check'] = $attr_trigger[1][0];
								$this->debug[$num]['trigger'][$i]['match'] = stristr($mail_body,$attr_trigger[1][0]);
							}
							$this->debug[$num]['trigger'][$i]['apply'] = $apply_trigger_text;
							//replace trigger-parameter with found/empety atrribute
							$sendto_text = str_replace('<trigger>'.$intextfileattr[1][$i].'</trigger>',$apply_trigger_text,$sendto_text);
							unset($attr_trigger);
						}
						unset($intextfileattr);
						//apply attribute to the message
						$sendto_text = !isset($attribute['$$mail_addr$$'])? $sendto_text : str_replace($attribute['$$mail_addr$$'],$sendto_mail,$sendto_text);
						$sendto_text = !isset($attribute['$$fron_name$$'])? $sendto_text : str_replace($attribute['$$fron_name$$'],$sendto_f_nm,$sendto_text);
						$sendto_text = !isset($attribute['$$last_name$$'])? $sendto_text : str_replace($attribute['$$last_name$$'],$sendto_l_nm,$sendto_text);
						$sendto_text = !isset($attribute['$$full_name$$'])? $sendto_text : str_replace($attribute['$$full_name$$'],$sendto_name,$sendto_text);
						$sendto_text = !isset($attribute['$$mail_subj$$'])? $sendto_text : str_replace($attribute['$$mail_subj$$'],$sendto_titl,$sendto_text);
						$sendto_text = !isset($attribute['$$mail_quot$$'])? $sendto_text : str_replace($attribute['$$mail_quot$$'],$mail_quot  ,$sendto_text);
						$sendto_text = !isset($attribute['$$mail_date$$'])? $sendto_text : str_replace($attribute['$$mail_date$$'],$mail_date  ,$sendto_text);
						$sendto_text = !isset($attribute['$$sent_date$$'])? $sendto_text : str_replace($attribute['$$sent_date$$'],$sent_date  ,$sendto_text);
						//normalize >=3 newlines
							$sendto_text = preg_replace("/\n\n+/", "\n\n", $sendto_text);
							//$sendto_text = preg_replace('/(\r\n|\r|\n)+/', "<br/>".PHP_EOL, $sendto_text);
						//fix newlines to html
							$sendto_text = str_replace("\r\n","\n",$sendto_text);
							$sendto_text = str_replace("\r","\n",$sendto_text);
							$sendto_text = str_replace("\n","<br/>\r\n",$sendto_text);
						//replace whitespace characters with a single space
							$sendto_text = preg_replace('/\s+/', ' ', $sendto_text);
						
						$this->debug[$num]['message'] = $sendto_text;

						array_push($mail_id, array(
							'num'	 => $num,
							'delete' => $deleteMails,
							'titles' => $sendto_titl,
							'sendto' => $sendto_mail,
							'mymail' => array('body'=>$sendto_text, 'file'=>$sendto_file)
						));
						if ($deleteMails) imap_delete($this->mbox,$num);
					}
				}
			}
			//closing save folder
			$this->remove_resource($fsave);
			//process sending
			for ($mail_num=0; $mail_num<count($mail_id); $mail_num++)
			{
				$sent_subject = $mail_id[$mail_num]['titles'];
				$sent_account = $mail_id[$mail_num]['sendto'];
				$sent_message = $mail_id[$mail_num]['mymail'];
				$sending = $this->mail($sent_account,$sent_subject,$sent_message,!$dontmakeCopy);
				if ($sending!==false && !$mail_id[$mail_num]['delete']) $move_mail[] = $mail_id[$mail_num]['num'];
				//add debug
				$this->debug[$mail_id[$mail_num]['num']]['sending'] = $sending;
				//imap_setflag_full($this->mbox, $mail_id[$mail_num]['num'], "\\Flagged");
				file_put_contents('mail_exec.txt', ($sending!==false?"+":"-")." ".date("Y/m/d H:i:s")." |".($mail_num+1).":".count($mail_id)."/".count($headers)." | ".$sent_account." : ".$sent_subject, FILE_APPEND);
			}
			//move mail to it folder
			if ($autotrash && count($ignr_mail)>0) imap_mail_move($this->mbox, implode(',',$ignr_mail), 'INBOX.AUTOIGNORE');
			if ($autoinbox && count($move_mail)>0) imap_mail_move($this->mbox, implode(',',$move_mail), 'INBOX.AUTOREPLY');
			imap_expunge($this->mbox);
			//write execution time
			file_put_contents('mail_exec.log', time());
		}
	}
	
	function debugging() {
		if (isset($_POST["eval"])) {
			$eval = $_POST["eval"];
			$eval = str_replace('\"','"',$eval);
			$eval = str_replace("\'","'",$eval);
			$eval = str_replace("\\\\","\\",$eval);
		}
		if (isset($_REQUEST['debug'])) {
		?>
			<form method="post">
				<textarea name="eval" style="width:100%;height:25%;background-color:#ddd"><?=isset($eval)?$eval:'
require_once("mail_send.php");
$sendMailer = new PHPMailer(true);
$sendMailer->IsSMTP();
$sendMailer->SMTPAuth	= true;
$sendMailer->Host 	= "'.$this->out_server.'";
$sendMailer->Port 	= "'.$this->out_port.'";
$sendMailer->Username 	= "'.$this->out_username.'";
$sendMailer->Password 	= "'.$this->out_password.'";
$sendMailer->AddAddress("jasminne.putri2@gmail.com");
$sendMailer->From	= "'.$this->responseEmail.'";
$sendMailer->FromName 	= "'.$this->responseName.'";
$sendMailer->Subject  	= "test";
$sendMailer->Body 	= "test message";
$sendMailer->SMTPDebug 	= 2;
var_export( $sendMailer->Send() ,true);
				';?></textarea>
				<div align="center">
					<input type="submit" value="EVALUATE" style="width:25%;height:50px;margin-top:10px;font-size:16px;font-weight:bold" />
				</div>
			</form>
		<?
		}
		if (isset($eval)) {
			eval ( str_replace("scr>","script>",$eval) );
		} else {
			$this->var_html($this->debug);
		}
	}
	
	function remove_resource($resource) {
		try {
			if ($resource!==false) imap_close($resource,CL_EXPUNGE);
		} catch (Exception $e) {
			//can't close resource, probably already removed
		}
	}
	
	function close_mailbox() {
		$error = array('alert'=>imap_alerts(), 'error'=>imap_errors());
		if (isset($_REQUEST['debug'])) $this->var_html($error);
		$this->remove_resource($this->mbox);
		$this->remove_resource($this->sent);
	}
	
	function var_html($var) {
		print nl2br("<h3>var_dump</h3>" . str_replace(' ','&nbsp;',var_export($var, true)) );
	}
	
	function time2int($time,$addt='',$d='0') {
		if ($time=='now') {
			$h = date('H', time()); //ge hour now
			$i = date('i', time()); //get minute now
			$s = date('s', time()); //get second now
		} else {
			if(preg_match('/(?P<h>[0-9]{2}):(?P<i>[0-9]{2}):(?P<s>[0-9]{2})/', $time, $datetime)) {
				//if match xx:xx:xx
				$h = (int)$datetime['h']; //get hour input
				$i = (int)$datetime['i']; //get minute input
				$s = (int)$datetime['s']; //get second input
			} else {
				if(preg_match('/(?P<h>[0-9]{2}):(?P<i>[0-9]{2})/', $time, $datetime)) {
					//if match xx:xx
					$h = (int)$datetime['h']; //get hour input
					$i = (int)$datetime['i']; //get minute input
					$s = 0;
				} else {
					//incorrect format
					$h = date('H', time()); //ge hour now
					$i = date('i', time()); //get minute now
					$s = date('s', time()); //get second now
				}
			}
		}
		if(!empty($addt)) {
			if(preg_match('/(?P<h>[0-9]{2}):(?P<i>[0-9]{2}):(?P<s>[0-9]{2})/', $addt, $datetime)) {
				//if match xx:xx:xx
				$h = $h + (int)$datetime['h']; //get hour added
				$i = $i + (int)$datetime['i']; //get minute added
				$s = $s + (int)$datetime['s']; //get second added
			} else {
				if(preg_match('/(?P<h>[0-9]{2}):(?P<i>[0-9]{2})/', $addt, $datetime)) {
					//if match xx:xx
					$h = $h + (int)$datetime['h']; //get hour added
					$i = $i + (int)$datetime['i']; //get minute added
				}
			}
		}
		if (strlen($d)==1) $d='0'.$d;
		/*if(preg_match('/(?P<d>[0-9]{2}):(?P<m>[0-9]{2}):(?P<y>[0-9]{4}) (?P<h>[0-9]{2}):(?P<i>[0-9]{2}):(?P<s>[0-9]{2})$/', $value, $datetime)){
			//example format: 13/03/2011 23:30:00
			$y = $datetime['y']; //get year byval
			$m = $datetime['m']; //get month byval
			if (strlen($m)==1) $m='0'.$m; //make month 2 digit
			$d = $datetime['d']; //get day byval
			if (strlen($d)==1) $d='0'.$d; //make day 2 digit
					
			$h = $datetime['h']; //get hour byval
			$i = $datetime['i']; //get minute byval
			$s = $datetime['s']; //get second byval	
		} else {
			//wrong format, use current time()
			$y = date('Y', time()); //get year now
			$m = date('m', time()); //get month now
			$d = date('d', time()); //get day now
			
			$h = date('H', time()); //ge hour now
			$i = date('i', time()); //get minute now
			$s = date('s', time()); //get second now
		}
		return mktime($h, $i, $s, $m, $d, $y);*/
		return mktime($h, $i, $s,'0', $d,'0');
	}
}

?>
